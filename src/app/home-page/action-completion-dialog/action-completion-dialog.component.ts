import {Component, OnInit} from '@angular/core';
import {MatDialogRef} from '@angular/material/dialog';

@Component({
  templateUrl: './action-completion-dialog.component.html'
})

export class ActionCompletionDialogComponent implements OnInit {
  constructor(public matDialogRef: MatDialogRef<ActionCompletionDialogComponent>) {
  }

  public ngOnInit(): void {
    this.matDialogRef.updateSize('600px');

    this.matDialogRef.disableClose = true;
  }

  public closeDialog(): void {
    this.matDialogRef.close();
  }
}
