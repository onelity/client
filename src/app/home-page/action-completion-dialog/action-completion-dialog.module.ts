import {ActionCompletionDialogComponent} from './action-completion-dialog.component';
import {CommonModule} from '@angular/common';
import {MaterialModule} from '../../shared/material.module';
import {NgModule} from '@angular/core';

@NgModule({
  declarations: [ActionCompletionDialogComponent],
  entryComponents: [ActionCompletionDialogComponent],
  imports: [
    CommonModule,
    MaterialModule
  ]
})

export class ActionCompletionDialogModule {
}
