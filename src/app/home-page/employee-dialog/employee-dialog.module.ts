import {CommonModule} from '@angular/common';
import {EmailValidators} from '../../validators/email.validators';
import {EmployeeDialogComponent} from './employee-dialog.component';
import {EmployeeService} from '../../shared/services/employee.service';
import {FormService} from '../../shared/services/form.service';
import {MaterialModule} from '../../shared/material.module';
import {NgModule} from '@angular/core';
import {NgxMaskModule} from 'ngx-mask';
import {ReactiveFormsModule} from '@angular/forms';

@NgModule({
  declarations: [EmployeeDialogComponent],
  entryComponents: [EmployeeDialogComponent],
  imports: [
    CommonModule,
    MaterialModule,
    NgxMaskModule.forRoot(),
    ReactiveFormsModule
  ],
  providers: [
    EmailValidators,
    EmployeeService,
    FormService
  ]
})

export class EmployeeDialogModule {
}
