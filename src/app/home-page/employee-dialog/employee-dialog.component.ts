import {Component, Inject} from '@angular/core';
import {EmailValidators} from '../../validators/email.validators';
import {EmployeeService} from '../../shared/services/employee.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {FormService} from '../../shared/services/form.service';
import {LoadingService} from '../../shared/services/loading.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
  templateUrl: './employee-dialog.component.html'
})

export class EmployeeDialogComponent {
  public employee: any;
  public employeeFormGroup: FormGroup;

  constructor(private emailValidators: EmailValidators,
              private employeeService: EmployeeService,
              public formService: FormService,
              private loadingService: LoadingService,
              private matDialogRef: MatDialogRef<EmployeeDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) {
    this.employee = this.data.employee;

    this.employeeFormGroup = new FormGroup({
      address: new FormControl(this.employee?.address, Validators.required),
      email: new FormControl(this.employee?.email, [Validators.email, Validators.required],
        this.emailValidators.uniqueEmail(this.employee?.email)),
      firstName: new FormControl(this.employee?.firstName, Validators.required),
      lastName: new FormControl(this.employee?.lastName, Validators.required),
      mobile: new FormControl(this.employee?.mobile, [
        Validators.minLength(10),
        Validators.required
      ])
    });
  }

  public closeDialog(): void {
    this.matDialogRef.close();
  }

  public isFormInvalid(): boolean {
    return this.employeeFormGroup.invalid;
  }

  public setButtonText(): string {
    return this.employee?.id ? 'Ενημέρωση' : 'Καταχώρηση';
  }

  public setTitle(): string {
    const mode = this.employee?.id ? 'Επεξεργασία' : 'Προσθήκη';

    return `${mode} Υπαλλήλου`;
  }

  public async submitEmployee(): Promise<any> {
    try {
      this.loadingService.show();

      const employee = Object.assign({}, this.employeeFormGroup.value);

      if (this.employee?.id) {
        await this.employeeService.updateEmployee(this.employee.id, employee);
      } else {
        await this.employeeService.insertEmployee(employee);
      }

      this.matDialogRef.close('success');
    } catch (error) {
      throw error;
    } finally {
      this.loadingService.hide();
    }
  }
}
