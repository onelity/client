import {ActionCompletionDialogComponent} from './action-completion-dialog/action-completion-dialog.component';
import {BehaviorSubject} from 'rxjs';
import {Column} from '../models/column';
import {Component, OnInit} from '@angular/core';
import {ConfirmActionDialogComponent} from './confirm-action-dialog/confirm-action-dialog.component';
import {EmployeeDialogComponent} from './employee-dialog/employee-dialog.component';
import {EmployeeService} from '../shared/services/employee.service';
import {Employee} from '../models/employee';
import {LoadingService} from '../shared/services/loading.service';
import {MatDialog} from '@angular/material/dialog';

@Component({
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})

export class HomePageComponent implements OnInit {
  public columns: Column[];
  public dataSource: BehaviorSubject<Employee[]>;
  public displayedColumns: string[];

  constructor(private employeeService: EmployeeService,
              private loadingService: LoadingService,
              private matDialog: MatDialog) {
    this.columns = [
      {
        name: 'firstName',
        title: 'Όνομα'
      },
      {
        name: 'lastName',
        title: 'Επώνυμο'
      },
      {
        name: 'email',
        title: 'Email'
      },
      {
        name: 'address',
        title: 'Διεύθυνση'
      },
      {
        name: 'mobile',
        title: 'Κινητό'
      }
    ];

    this.dataSource = new BehaviorSubject<any[]>([]);

    this.displayedColumns = this.columns.map(column => column.name);

    this.displayedColumns.push('actions');
  }

  public async ngOnInit(): Promise<any> {
    try {
      await this.updateData();
    } catch (error) {
      throw error;
    }
  }

  public deleteEmployee(employee: Employee): void {
    this.matDialog.open(ConfirmActionDialogComponent, {
      data: {
        confirmFunction: async () => {
          try {
            this.loadingService.show();

            await this.employeeService.deleteEmployee(employee.id);

            const actionCompletionDialogReference = this.matDialog.open(ActionCompletionDialogComponent, {});

            actionCompletionDialogReference.afterClosed().subscribe(async () => {
              await this.updateData();
            });
          } catch (error) {
            throw error;
          } finally {
            this.loadingService.hide();
          }
        },
        message: 'Είστε σίγουροι πως θέλετε να διαγράψετε τον Εργαζόμενο;',
        title: 'Διαγραφή Εργαζομένου'
      }
    });
  }

  public openEmployeeDialog(employee?: Employee): void {
    const employeeDialogReference = this.matDialog.open(EmployeeDialogComponent, {
      data: {
        employee
      }
    });

    employeeDialogReference.afterClosed().subscribe(async (result) => {
      if (result) {
        const actionCompletionDialogReference = this.matDialog.open(ActionCompletionDialogComponent, {});

        actionCompletionDialogReference.afterClosed().subscribe(async () => {
          await this.updateData();
        });
      }
    });
  }

  public setValue(value: any, column: any): string {
    const cellValue = value[column.name];

    if (cellValue !== undefined) {
      return cellValue ? cellValue : '-';
    } else {
      return '-';
    }
  }

  private async updateData(): Promise<any> {
    try {
      this.loadingService.show();

      const data = await this.employeeService.getEmployees();

      this.dataSource = new BehaviorSubject<Employee[]>(data);
    } catch (error) {
      throw error;
    } finally {
      this.loadingService.hide();
    }
  }
}
