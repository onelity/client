import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
  templateUrl: './confirm-action-dialog.component.html'
})

export class ConfirmActionDialogComponent implements OnInit {
  private readonly confirmFunction: () => void;
  public message: string;
  public title: string;

  constructor(private matDialogRef: MatDialogRef<ConfirmActionDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) {
    this.confirmFunction = this.data.confirmFunction;

    this.message = this.data.message;

    this.title = this.data.title;
  }

  public ngOnInit(): void {
    this.matDialogRef.updateSize('600px');
  }

  public closeDialog(): void {
    this.matDialogRef.close();
  }

  public confirmAction(): void {
    this.matDialogRef.close();

    this.confirmFunction();
  }
}
