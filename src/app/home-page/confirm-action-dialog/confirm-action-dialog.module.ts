import {CommonModule} from '@angular/common';
import {ConfirmActionDialogComponent} from './confirm-action-dialog.component';
import {MaterialModule} from '../../shared/material.module';
import {NgModule} from '@angular/core';

@NgModule({
  declarations: [ConfirmActionDialogComponent],
  entryComponents: [ConfirmActionDialogComponent],
  imports: [
    CommonModule,
    MaterialModule
  ]
})

export class ConfirmActionDialogModule {
}
