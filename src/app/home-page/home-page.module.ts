import {ActionCompletionDialogModule} from './action-completion-dialog/action-completion-dialog.module';
import {CommonModule} from '@angular/common';
import {ConfirmActionDialogModule} from './confirm-action-dialog/confirm-action-dialog.module';
import {EmployeeDialogModule} from './employee-dialog/employee-dialog.module';
import {EmployeeService} from '../shared/services/employee.service';
import {HomePageComponent} from './home-page.component';
import {HomePageRoutingModule} from './home-page-routing.module';
import {MaterialModule} from '../shared/material.module';
import {NgModule} from '@angular/core';

@NgModule({
  declarations: [HomePageComponent],
  imports: [
    ActionCompletionDialogModule,
    CommonModule,
    ConfirmActionDialogModule,
    EmployeeDialogModule,
    HomePageRoutingModule,
    MaterialModule
  ],
  providers: [EmployeeService]
})

export class HomePageModule {
}
