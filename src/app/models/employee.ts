export interface Employee {
  address: string;
  email: string;
  firstName: string;
  id: number;
  lastName: string;
  mobile: string;
}
