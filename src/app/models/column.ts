export interface Column {
  name: string;
  title: string;
}
