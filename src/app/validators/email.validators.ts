import {AbstractControl, AsyncValidatorFn, ValidationErrors} from '@angular/forms';
import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';

@Injectable()
export class EmailValidators {
  constructor(private httpClient: HttpClient) {
  }

  public uniqueEmail(email?: string): AsyncValidatorFn {
    return (control: AbstractControl): Promise<ValidationErrors | null> => {
      return new Promise((resolve) => {
        setTimeout(() => {
          this.httpClient.get(`employee/email/${control.value}`).toPromise().then((employees: any) => {
            if (email) {
              if ((employees.length === 1 && employees[0].email === email) || employees.length === 0) {
                resolve(null);
              } else {
                resolve({uniqueEmail: true});
              }
            } else if (employees.length > 0) {
              resolve({uniqueEmail: true});
            } else {
              resolve(null);
            }
          });
        }, 1000);
      });
    };
  }
}
