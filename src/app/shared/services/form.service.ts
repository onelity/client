import {FormGroup} from '@angular/forms';
import {Injectable} from '@angular/core';

@Injectable()
export class FormService {
  constructor() {
  }

  public isFormFieldValid(formField: string, formGroup: FormGroup): boolean {
    const value = formGroup.get(formField);

    if (value) {
      return value.touched && value.invalid;
    } else {
      return false;
    }
  }

  // @ts-ignore
  public setErrorMessage(formField: string, formGroup: FormGroup, type?: string): string {
    const errors = formGroup.get(formField)?.errors;

    switch (type) {
      case 'email':
        if (errors?.required) {
          return 'Υποχρεωτικό πεδίο.';
        }

        if (errors?.email) {
          return 'Λάθος τύπος email.';
        }

        break;
      case 'telephone':
        if (errors?.required) {
          return 'Υποχρεωτικό πεδίο.';
        }

        if (errors?.minlength) {
          return 'Συμπληρώστε 10 ψηφία.';
        }

        break;
      case 'uniqueEmail':
        if (errors?.required) {
          return 'Υποχρεωτικό πεδίο.';
        }

        if (errors?.uniqueEmail) {
          return 'Υπάρχει εργαζόμενος με το ίδιο email.';
        }

        break;
      default:
        if (errors?.required) {
          return 'Υποχρεωτικό πεδίο.';
        }

        break;
    }
  }
}
