import {BehaviorSubject, Observable} from 'rxjs';
import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoadingService {
  private spinnerEvents: BehaviorSubject<string>;

  constructor() {
    this.spinnerEvents = new BehaviorSubject<string>('show');
  }

  public hide(): void {
    this.spinnerEvents.next('hide');
  }

  public show(): void {
    this.spinnerEvents.next('show');
  }

  get events(): Observable<string> {
    return this.spinnerEvents.asObservable();
  }
}
