import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DatabaseService {
  private headers: any;

  constructor(private httpClient: HttpClient) {
    this.headers = new HttpHeaders();
  }

  public deleteRequest(url: string): Promise<any> {
    return new Promise((resolve, reject) => {
      this.httpClient.delete(url).subscribe((response) => {
        resolve(response);
      }, (error) => {
        reject(error);
      });
    });
  }

  public getRequest(url: string): Promise<any> {
    return new Promise((resolve, reject) => {
      this.httpClient.get(url).subscribe((response) => {
        resolve(response);
      }, (error) => {
        reject(error);
      });
    });
  }

  public postRequest(object: any, url: string): Promise<any> {
    return new Promise((resolve, reject) => {
      this.httpClient.post(url, object).subscribe((response) => {
        resolve(response);
      }, (error) => {
        reject(error);
      });
    });
  }

  public putRequest(object: any, url: string): Promise<any> {
    return new Promise((resolve, reject) => {
      this.httpClient.put(url, object).subscribe((response) => {
        resolve(response);
      }, (error) => {
        reject(error);
      });
    });
  }
}
