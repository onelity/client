import {DatabaseService} from '../../shared/services/database.service';
import {Injectable} from '@angular/core';

@Injectable()
export class EmployeeService {
  private readonly serviceModel: string;

  constructor(private databaseService: DatabaseService) {
    this.serviceModel = 'employee';
  }

  public async deleteEmployee(id: number): Promise<any> {
    try {
      return await this.databaseService.deleteRequest(`${this.serviceModel}/${id}`);
    } catch (error) {
      throw error;
    }
  }

  public async getEmployee(id: number): Promise<any> {
    try {
      return await this.databaseService.getRequest(`${this.serviceModel}/${id}`);
    } catch (error) {
      throw error;
    }
  }

  public async getEmployees(): Promise<any> {
    try {
      return await this.databaseService.getRequest('employees');
    } catch (error) {
      throw error;
    }
  }

  public async insertEmployee(employee: any): Promise<any> {
    try {
      return await this.databaseService.postRequest(employee, this.serviceModel);
    } catch (error) {
      throw error;
    }
  }

  public async updateEmployee(id: number, employee: any): Promise<any> {
    try {
      return await this.databaseService.putRequest(employee, `${this.serviceModel}/${id}`);
    } catch (error) {
      throw error;
    }
  }
}
