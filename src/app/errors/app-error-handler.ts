import {ErrorDialogComponent} from './error-dialog/error-dialog.component';
import {ErrorHandler, Injectable} from '@angular/core';
import {LoadingService} from '../shared/services/loading.service';
import {MatDialog} from '@angular/material/dialog';

@Injectable()
export class AppErrorHandler implements ErrorHandler {
  constructor(private loadingService: LoadingService,
              private matDialog: MatDialog) {
  }

  handleError(error: any): void {
    console.log(error);

    this.loadingService.hide();

    this.matDialog.open(ErrorDialogComponent, {
      data: {
        message: 'Παρουσιάστηκε κάποιο σφάλμα κατά την εκτέλεση κάποιας διεργασίας του συστήματος.',
        title: 'Ειδοποίηση Συστήματος'
      }
    });
  }
}
