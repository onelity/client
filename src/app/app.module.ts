import {AppComponent} from './app.component';
import {AppErrorHandler} from './errors/app-error-handler';
import {AppRoutingModule} from './app-routing.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {BrowserModule} from '@angular/platform-browser';
import {ErrorDialogComponent} from './errors/error-dialog/error-dialog.component';
import {ErrorHandler, NgModule} from '@angular/core';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {HashLocationStrategy, LocationStrategy} from '@angular/common';
import {InterceptorService} from './interceptors/interceptor.service';
import {LoadingComponent} from './shared/components/loading/loading.component';
import {MaterialModule} from './shared/material.module';
import {NotFoundPageComponent} from './errors/not-found-page/not-found-page.component';

@NgModule({
  bootstrap: [AppComponent],
  declarations: [
    AppComponent,
    ErrorDialogComponent,
    LoadingComponent,
    NotFoundPageComponent
  ],
  entryComponents: [ErrorDialogComponent],
  imports: [
    AppRoutingModule,
    BrowserAnimationsModule,
    BrowserModule,
    HttpClientModule,
    MaterialModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS, useClass: InterceptorService, multi: true
    },
    {
      provide: ErrorHandler, useClass: AppErrorHandler
    },
    {
      provide: LocationStrategy, useClass: HashLocationStrategy
    }
  ],
})

export class AppModule {
}
